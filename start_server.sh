#!/bin/sh

# read env
USERNAME=${DJ_USERNAME:-username}
PASSWORD=${DJ_PASSWORD:-password}

sleep 10

scrapyd-deploy --build-egg output.egg

python manage.py migrate
# Please change password !
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('$USERNAME', 'admin@myproject.com', '$PASSWORD')" | python manage.py shell
python manage.py runserver 0.0.0.0:8000