from django.contrib import admin
from .models import Movie, Weibo, CrawlState

# Register your models here.
class MovieAdmin(admin.ModelAdmin):

    '''设置列表可显示的字段'''
    list_display = ('uid', 'title', 'release', 'url')

    '''设置过滤选项'''
    list_filter = ('release',)

    '''每页显示条目数'''
    list_per_page = 50

    '''设置可编辑字段'''
    list_editable = ('title',)

    # '''按日期月份筛选'''
    # date_hierarchy = 'pub_date'

    # '''按发布日期排序'''
    # ordering = ('-mod_date',)

admin.site.register(Movie, MovieAdmin)

class WeiboAdmin(admin.ModelAdmin):

    '''设置列表可显示的字段'''
    list_display = ('uid', 'title', 'date')

    '''设置过滤选项'''
    list_filter = ('date',)

    '''每页显示条目数'''
    list_per_page = 50

    '''设置可编辑字段'''
    list_editable = ('title',)

admin.site.register(Weibo, WeiboAdmin)

class CrawlStateAdmin(admin.ModelAdmin):

    '''设置列表可显示的字段'''
    list_display = ('crawler_name', 'current_page')

    '''每页显示条目数'''
    list_per_page = 50

    '''设置可编辑字段'''
    list_editable = ('current_page',)

admin.site.register(CrawlState, CrawlStateAdmin)
