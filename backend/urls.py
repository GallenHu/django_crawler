from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r's8/', views.S8List.as_view(), name='s8_api'),
    url(r'weibo/', views.WeiboList.as_view(), name='weibo_api'),
    url(r'douban/', views.DoubanList.as_view(), name='douban_api'),
    url(r'test/', views.test, name='api')
]