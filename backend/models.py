from django.db import models

# Create your models here.
class Movie(models.Model):
    """
    电影表
    """

    def __str__(self):
        return "%s"%(self.title)

    uid = models.CharField('唯一ID', max_length=20, unique=True, db_index=True)
    title = models.CharField('标题', max_length=190)
    release = models.CharField('上映时间', max_length=20)
    url = models.CharField('链接', max_length=190)

class Weibo(models.Model):
    """
    weibo表
    """

    def __str__(self):
        return "%s"%(self.title)

    uid = models.CharField('唯一ID', max_length=50, unique=True, db_index=True)
    title = models.CharField('标题', max_length=190)
    hot = models.IntegerField('热度', default=0)
    date = models.DateTimeField('时间', null=True)
    durations = models.IntegerField('持续分钟', default=0)


class S8(models.Model):
    """
    s8表
    """

    def __str__(self):
        return "%s"%(self.title)

    uid = models.CharField('唯一ID', max_length=50, unique=True, db_index=True)
    title = models.CharField('标题', max_length=190)
    content = models.CharField('内容', max_length=190, default='null')
    img_content = models.CharField('图片内容', max_length=190, default='')
    imgs = models.CharField('插图', max_length=190)
    post_date = models.CharField('发布日期', max_length=50)
    post_time = models.CharField('发布时间', max_length=50)
    post_author = models.CharField('作者', max_length=190)
    crawl_date = models.DateTimeField('爬取时间', null=True)
    view_times = models.CharField('查看次数', max_length=10)


class CrawlState(models.Model):
    """
    抓取状态表
    """

    def __str__(self):
        return "%s"%(self.crawler_name)

    uid = models.CharField('唯一ID', max_length=20, unique=True, db_index=True)
    crawler_name = models.CharField('爬虫名', max_length=190)
    # 使用 CharField 以支持类似 2020-12-31 这样的"页码"
    current_page = models.CharField('页码', max_length=20, null=True)
