from rest_framework import serializers
from . import models

class DoubanSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('uid', 'title',)
        model = models.Movie

class WeiboSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('uid', 'title', 'hot', 'date', 'durations')
        model = models.Weibo

class S8Serializer(serializers.ModelSerializer):
    class Meta:
        fields = ('uid', 'title', 'imgs', 'content', 'img_content', 'post_date', 'crawl_date')
        model = models.S8
