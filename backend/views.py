from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics, filters
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from . import models
from . import serializers

# Create your views here.
def index(request):
    return HttpResponse('hello world')

def test(request):
    return HttpResponse('hello api')

class DoubanList(generics.ListAPIView):
    queryset = models.Movie.objects.all()
    serializer_class = serializers.DoubanSerializer

class WeiboList(generics.ListAPIView):
    queryset = models.Weibo.objects.all()
    serializer_class = serializers.WeiboSerializer

    # backend 中加入OrderingFilter SearchFilter 激活ordering filter，字段为ordering
    filter_backends = (filters.OrderingFilter, filters.SearchFilter,)
    ordering_fields = ('date', 'hot',)
    # 指定默认的排序字段
    ordering = ('date',)
    # 指定可搜索字段
    search_fields = ('date', 'title')

class S8List(generics.ListAPIView):
    # BasicAuthentication need!
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = models.S8.objects.all()
    serializer_class = serializers.S8Serializer

    # backend 中加入OrderingFilter SearchFilter 激活ordering filter，字段为ordering
    filter_backends = (filters.OrderingFilter, filters.SearchFilter,)
    ordering_fields = ('post_date', 'crawl_date')
    # 指定默认的排序字段
    ordering = ('-post_date',)
    # 指定可搜索字段
    search_fields = ('title',)
