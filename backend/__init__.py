import pymysql, sys, logging
pymysql.version_info = (1, 4, 2, "final", 0)
pymysql.install_as_MySQLdb()

# check db connection
from django.db import connections
from django.db.utils import OperationalError
db_conn = connections['default']
try:
    c = db_conn.cursor()
except OperationalError:
    logging.error('db connect error!!!')
    sys.exit(1)
else:
    c.close()
