# Generated by Django 3.1.4 on 2020-12-30 08:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uid', models.CharField(db_index=True, max_length=20, unique=True, verbose_name='唯一ID')),
                ('title', models.CharField(max_length=190, verbose_name='标题')),
                ('release', models.CharField(max_length=20, verbose_name='上映时间')),
                ('url', models.CharField(max_length=190, verbose_name='链接')),
            ],
        ),
    ]
