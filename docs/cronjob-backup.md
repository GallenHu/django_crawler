> 整个项目数据是核心，完全备份 SQL 就完整备份了数据！

## 定时爬取

```sh
mkdir /home/shells
cd /home/shells
vim weibo_crawl.sh
vim s8_crawl.sh

crontab -e
```

weibo_crawl.sh

```sh
curl http://127.0.0.1:16800/schedule.json -d project=default -d spider=weibo
```

s8_crawl.sh

```sh
curl http://127.0.0.1:16800/schedule.json -d project=default -d spider=s8
```

crontab

```ini
# 每2小时第1分钟
1 */2 * * * /bin/bash /home/shells/weibo_crawl.sh

# 每天3点和14点
0 3,14 * * * /bin/bash /home/shells/s8_crawl.sh
```

## 数据库备份

```sh
vim /home/shells/backsql.sh

# 进入备份目录,即git目录
mkdir /home/backup-sql
cd /home/backup-sql
bash /home/shells/backsql.sh # 首次执行会git报错，进行git配置调试直到成功

# 脚本执行成功后加入 cron
```

backsql.sh

```sh
#!/bin/bash
# Variables
# NOT end with /
backupdir=/home/backup-sql
time=` date +%Y%m%d%H%M%S `
# change me
dockerapp=djapp-mysql
# change me
passwd=some_django_app
# change me
dbname=django_app

########## start

docker exec $dockerapp sh -c "exec mysqldump -uroot -p$passwd $dbname | gzip" > $backupdir/database$time.sql.gz

# remove files after 7 days
find $backupdir -name "database*.sql.gz" -type f -mtime +7 -exec rm {} \; > /dev/null 2>&1

# git
cd $backupdir
git add -A
git config user.email "gallenmbp@gmail.com"
git config user.name "Auto-Job-Server"
git commit -m $time
git push origin master
```

crontab

```ini
# 每天2点30分
30 2 * * * /bin/bash /home/shells/backsql.sh
```