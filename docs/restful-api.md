## api

weibo

```
https://api.domain.com/api/weibo/?format=json&search=2021-03-16&ordering=-hot&limit=50&offset=0
```

## Nginx conf

```
server {
        listen 80;
        listen 443 ssl;
        server_name api.domain.com;
        index index.html index.htm index.php default.html default.htm default.php;

        ssl_certificate /path/to/ssl-chain.crt;
        ssl_certificate_key /path/to/ssl.key;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers AES256+EECDH:AES256+EDH:!aNULL;

        location / {
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_set_header X-NginX-Proxy true;
            proxy_pass  http://1.1.1.1:18000/;
            proxy_redirect off;
        }
}
```
