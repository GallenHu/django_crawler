## (重新)部署

1. 创建目录

```sh
mkdir /home/djcrawler
mkdir /home/mysql_data
cd /home/djcrawler
```

2. 查看源代码，确认 `项目根目录/djcrawler/settings.py` 中的配置（CORS_ALLOWED）符合要求
(如果有问题，请修改<托管于gitlab,同步到bitbucket的>源代码然后通过docker hub自动build)

3. 查看源代码，创建 env, docker-compose.yml 文件

在 `/home/djcrawler` 目录中

```sh
# 内容复制并修改: 项目根目录/djcrawler/.env.example
vim .env

# 内容复制: 项目根目录/docker-compose.yml
vim docker-compose.yml
```

4. 如果是第一次运行(没有 db 数据)

- 修改 docker-compose.yml 把 phpadmin 端口放开
- 注释掉 app 先不运行主程序
- 启动 `docker-compose up`
- 通过 PHPmyadmin, 选择已自动创建的数据库 django_app， 导入数据库备份 sql.zip 文件
- 查看已爬取的状态: `backend_crawlstate` 表，数据没问题则往下
- control + c 停止 docker-compose 后 恢复 docker-compose.yml 中的 app

5. 后续运行(db 数据导入完成后)

- (注释掉 PHPmyadmin 的端口)
- `docker-compose up` 启动
- (如果数据库没连接上 control + c 停止 docker-compose 后 `docker-compose up` 重启)

6. 测试是否正常
- 访问 Django `localhost:18000`
- `control` + `c` 停止 docker-compose 后执行 `docker-compose up -d` 后台启动
- 测试 `docker exec -it djapp scrapy crawl douban` 是否正常，正常才继续往下
- 启动 scrapyd:
- `docker exec -it djapp scrapyd > ./scrapyd.log &`
- 访问 Scrapyd 网页： `localhost:16800`

## 更新镜像

```sh
docker-compose down
docker pull hvanke/django_crawler
docker-compose up
```

## 停止服务

```sh
cd /home/djcrawler
docker-compose down
# 停止 cron job
crontab -e
```
