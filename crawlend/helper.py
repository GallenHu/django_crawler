import os
import sys
import django
import logging
import datetime
import moment


def beijing(sec, what):
    beijing_time = datetime.datetime.now() + datetime.timedelta(hours=8)
    return beijing_time.timetuple()


class CrawerHelper(object):

    def init_app(self):
        self.load_django_app()
        logging.Formatter.converter = beijing

    def load_django_app(self):

        BASE_DIR = os.path.dirname(os.path.dirname(
            os.path.dirname(os.path.abspath(__file__))))
        sys.path.append(BASE_DIR)

        # to use "backend"
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djcrawler.settings")
        django.setup()

    def get_current_page_from_db(self, uid):
        from backend.models import CrawlState

        try:
            crawlstate = CrawlState.objects.get(uid=uid)
            return crawlstate.current_page
        except CrawlState.DoesNotExist:
            return ''

    def save_current_page_to_db(self, uid, name, page):
        from backend.models import CrawlState

        crawlstate, created = CrawlState.objects.get_or_create(uid=uid)
        if created:
            # have created a new object
            crawlstate.crawler_name = name
            crawlstate.current_page = page
            crawlstate.save()
        else:
            # update
            crawlstate.current_page = page
            crawlstate.save()

    def local_date_to_datestr(self, local_date):
        return moment.date(local_date).format("YYYY-MM-DD")
