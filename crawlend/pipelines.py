# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import sys
import django
import logging
import traceback
from crawlend import state
from .helper import CrawerHelper

from backend.models import *


class CrawlendPipeline:
    def process_item(self, item, spider):
        return item


class DoubanDBPipeline(object):
    def process_item(self, item, spider):
        try:
            movie = Movie(uid=item['movie_uid'], title=item['movie_title'],
                          release=item['movie_release'], url=item['movie_url'])

            logging.warning('starting save... ' + item['movie_uid'])
            movie.save()
        # 插入错误
        except django.db.utils.IntegrityError:
            # repeat item
            logging.warning('Exist repeat douban item! ' + item['movie_uid'])

            state.douban_duplicate_count += 1

        except Exception as e:
            traceback.print_exc()
            sys.exit(1)
    pass


class WeiboDBPipeline(object):
    def save_page_to_db(self, pagestr):
        CrawerHelper().save_current_page_to_db(
            'weibo', '微博', CrawerHelper().local_date_to_datestr(pagestr))

    def process_item(self, item, spider):
        try:
            weibo = Weibo(uid=item['uid'], title=item['title'], hot=item['hot'],
                          date=item['date'], durations=item['durations'])

            logging.warning('starting save... ' + item['uid'])
            weibo.save()

            self.save_page_to_db(item['date'])

        # 插入错误
        except django.db.utils.IntegrityError:
            # repeat item
            logging.warning('Exist repeat weibo item! ' + item['uid'])

            state.weibo_duplicate_count += 1

            self.save_page_to_db(item['date'])

        except Exception as e:
            traceback.print_exc()
            sys.exit(1)
    pass


class S8DBPipeline(object):
    def save_page_to_db(self, pagestr):
        CrawerHelper().save_current_page_to_db('s8', 'S8', pagestr)

    def process_item(self, item, spider):
        try:
            s8 = S8(uid=item['uid'], title=item['title'], imgs=item['imgs'], content=item['content'], img_content=item['img_content'],
                    post_date=item['post_date'], post_time=item['post_time'], post_author=item['post_author'], crawl_date=item['crawl_date'], view_times=item['view_times'],)

            logging.warning('starting save... ' + item['uid'])
            s8.save()

            self.save_page_to_db(item['page'])

        # 插入错误
        except django.db.utils.IntegrityError:
            # repeat item
            logging.warning('Exist repeat S8 item! ' + item['uid'])

            state.s8_duplicate_count += 1

            self.save_page_to_db(item['page'])

        except Exception as e:
            traceback.print_exc()
            sys.exit(1)
    pass
