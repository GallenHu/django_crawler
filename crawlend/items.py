# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DoubanItem(scrapy.Item):
    movie_uid = scrapy.Field()
    movie_title = scrapy.Field()
    movie_release = scrapy.Field()
    movie_url = scrapy.Field()
    pass

class WeiboItem(scrapy.Item):
    uid = scrapy.Field()
    title = scrapy.Field()
    hot = scrapy.Field()
    date = scrapy.Field()
    durations = scrapy.Field()
    pass

class S8Item(scrapy.Item):
    uid = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    img_content = scrapy.Field()
    imgs = scrapy.Field()
    post_date = scrapy.Field()
    post_time = scrapy.Field()
    post_author = scrapy.Field()
    crawl_date = scrapy.Field()
    view_times = scrapy.Field()
    page = scrapy.Field()
    pass