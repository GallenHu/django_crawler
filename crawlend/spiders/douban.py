import scrapy
import logging
from crawlend.items import DoubanItem


class DoubanSpider(scrapy.Spider):
    name = 'douban'
    # allowed_domains = ['douban.com']
    # start_urls = ['http://douban.com/']
    custom_settings = {
        'ITEM_PIPELINES':{'crawlend.pipelines.DoubanDBPipeline': 300},
    }
    url = 'https://movie.douban.com/chart'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        'Origin': 'https://movie.douban.com',
        'Host': 'movie.douban.com',
        'Referer': 'https://movie.douban.com'
    }

    def start_requests(self):
        start_url = self.url
        logging.warn('ready to fetch: ' + start_url)
        yield scrapy.Request(url=start_url, headers=self.headers, callback=self.parse)

    def parse(self, response):
        res_list = response.css('.indent table')

        for record in res_list:
            item = DoubanItem()
            href = record.css('.pl2 a::attr(href)').get(default='').strip()
            href_splited = list(filter(None, href.split('/')))
            title = record.css('.pl2 a::text').get(default='').strip()

            if title.endswith('/'):
                title = title[:-1].strip()

            item['movie_uid'] = href_splited[-1]
            item['movie_title'] = title
            item['movie_release'] = 'test'
            item['movie_url'] = href

            yield item
        pass
