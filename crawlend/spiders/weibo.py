import scrapy
import logging
import traceback
import json
import moment
from datetime import datetime
from crawlend.items import WeiboItem
from crawlend.helper import CrawerHelper


class WeiboSpider(scrapy.Spider):
    name = 'weibo'
    # allowed_domains = ['weibo.com']
    # start_urls = ['http://weibo.com/']
    custom_settings = {
        'ITEM_PIPELINES': {'crawlend.pipelines.WeiboDBPipeline': 400},
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        'Origin': 'https://weibo.zhaoyizhe.com'
    }
    url = 'https://google-api.zhaoyizhe.com/google-api/index/mon/sec?date='
    start_page = '2019-10-25'
    end_page = moment.now().format("YYYY-MM-DD")
    ###### 从 DB 中获取 current_page!!! ######
    current_page = CrawerHelper().get_current_page_from_db('weibo')

    def start_requests(self):
        if self.current_page == '':
            self.current_page = self.start_page
        else:
            self.current_page = CrawerHelper().local_date_to_datestr(self.current_page)

        start_url = self.url + self.current_page
        logging.warning('ready to fetch: ' + start_url)
        yield scrapy.Request(url=start_url, headers=self.headers, callback=self.parse)

    def parse(self, response):
        try:
            jsonresponse = json.loads(response.text)
            if int(jsonresponse['code']) == 1:
                datalist = jsonresponse['data']
                for data in datalist:
                    item = WeiboItem()
                    item['uid'] = data['_id']
                    item['title'] = data['topic']
                    item['hot'] = data['hotNumber']
                    date = '20' + data['date'] + ':00' # 21-04-07 07:58 ---> 2021-04-07 07:58:00
                    item['date'] = date
                    item['durations'] = data['durations']
                    yield item

                # NOT <=
                if moment.date(self.current_page) < moment.date(self.end_page):
                    nextday =  moment.date(self.current_page).add(days=1).date
                    self.current_page = CrawerHelper().local_date_to_datestr(nextday)
                    nexturl = self.url + self.current_page
                    logging.warning('ready to fetch: ' + nexturl)
                    yield scrapy.Request(url=nexturl, headers=self.headers, callback=self.parse)

            else:
                logging.error('Error on get jsonresponse code!')
        except Exception as e:
            traceback.print_exc()
            logging.error('Error on parse response text!')
