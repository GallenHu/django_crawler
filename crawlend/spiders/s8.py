import scrapy
import logging
import traceback
import moment
from crawlend.items import S8Item


class S8Spider(scrapy.Spider):
    custom_settings = {
        'ITEM_PIPELINES': {'crawlend.pipelines.S8DBPipeline': 500},
    }

    name = 's8'
    base_url = 'http://sex8.cc/forum-103-' # http://sex8.cc/forum-103-9.html
    start_page = 1
    end_page = 100 # 300
    ###### 未保存/读取 DB 爬取页码!!!每次运行都是爬取 start_page 到 end_page ######
    current_page = start_page

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        'Origin': 'http://sex8.cc'
    }

    def gen_url(self, page_num):
        return self.base_url + str(page_num) + '.html'

    def start_requests(self):
        start_url = self.gen_url(self.start_page)
        logging.warning('ready to fetch: ' + start_url)
        yield scrapy.Request(url=start_url, headers=self.headers, callback=self.parse)

    def parse(self, response):
        try:
            list_selector = response.css('#threadlisttableid tbody')
            for index, post in enumerate(list_selector):
                item = S8Item()

                href = post.css('a.s.xst::attr(href)').get(default='').strip()
                uid = href.split('-')
                title = post.css('a.s.xst::text').get(default='').strip() # item
                if (len(uid) > 1):
                    uid = uid[1] # item
                else:
                    continue
                imgs = post.css('td.cl img.thread-img')
                imglist = []
                for index2, img in enumerate(imgs):
                    imgsrc = img.css('::attr(src)').get(default='').strip()
                    imglist.append(imgsrc)
                imgliststr = '$$$'.join(imglist) # item
                post_date = post.css('em span[title]::attr(title)').get(default='1970-12-30').strip() # item
                post_time = post.css('em span[title]::text').get(default='').strip() # item
                post_author = post.css('a[href*="space-uid"]').extract()
                post_author = ''.join(post_author) # item
                view_times = post.css('td.num em::text').get(default='1').strip() # item
                # DateTimeField 须使用 utc date
                crawl_date = moment.utcnow().format('YYYY-MM-DD HH:mm:ss') # item

                item['uid'] = uid
                item['title'] = title
                item['content'] = 'null'
                item['img_content'] = ''
                item['imgs'] = imgliststr
                item['post_date'] = moment.date(post_date).format("YYYY-MM-DD")
                item['post_time'] = post_time
                item['post_author'] = post_author
                item['crawl_date'] = crawl_date
                item['view_times'] = view_times
                item['page'] = self.current_page
                yield item

            if self.current_page < self.end_page:
                self.current_page = self.current_page + 1
                next_url = self.gen_url(self.current_page)
                logging.warning('ready to fetch: ' + next_url)
                yield scrapy.Request(url=next_url, headers=self.headers, callback=self.parse)
            pass

        except Exception as e:
            traceback.print_exc()
            logging.error('Error on parse response text!')
