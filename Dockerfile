FROM python:3.6-slim

COPY . /app
WORKDIR /app

# RUN pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple
RUN pip install -r requirements.txt

EXPOSE 8000

CMD sh start_server.sh
