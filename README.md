# Django_crawler

## env

```sh
pipenv --python 3.6.9
# 安装 requirements.txt
pipenv install
```

## server

```sh
# 启动 mysql,redis,phpmyadmin
docker-compose -f docker-compose.dev.yml up

# 激活Pipenv的终端(切换py版本)
pipenv shell
python manage.py migrate
python manage.py runserver
```

## crawl

```sh
# in scrapy.cfg folder
scrapy crawl douban
```

## requirements.txt

```sh
pipenv run pip freeze > requirements.txt
```

## Docker

```sh
docker build -t djapp .
```

## Scrapyd API

未指定项目名时，`project=default`

```sh
# docker-compose up
# visit UI: http://localhost:6800
curl http://localhost:6800/schedule.json -d project=default -d spider=douban
```

## Deploy

[docs](./docs/deploy.md)

## More Docs

[docs](./docs/)
